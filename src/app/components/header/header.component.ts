import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from '../../services/session-storage.service';
import { RouterService } from '../../services/router.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userNameFirstLetter: string = "";
  userLoggedIn: boolean = false;
  loggedInUserName: string;

  constructor(private sessionStorageService : SessionStorageService,
    private routerService : RouterService) {
    this.userNameFirstLetter = "M";
  }

  ngOnInit() {
    this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{
      console.log("Response from Behavior subject", loggedInUser);
      console.log("userid", loggedInUser['id']);
      
      if(loggedInUser['id']  !== undefined){
        this.userLoggedIn = true;
        this.loggedInUserName = loggedInUser['name'];
        this.userNameFirstLetter = this.loggedInUserName.charAt(0);
      }
      else{
        this.userLoggedIn = false;
      }
    })
   /* this.authenticationService.isUserLoggedIn().subscribe(res =>{
     
      
    })*/
  }

  routeToLogin(){
    this.routerService.routeToLogin();
  }

  routeToSignup(){
    this.routerService.routeToSignUp();
  }
  signout(){
    this.sessionStorageService.removeSessionStorageData();
    this.routerService.routeToLogin();

  }

}
