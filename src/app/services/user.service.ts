import { Injectable, OnInit } from '@angular/core';

import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { environment } from "src/environments/environment";
import { catchError, map, tap } from "rxjs/operators";
import { of } from "rxjs";
import { User } from "../models/user.model";


@Injectable({
  providedIn: 'root'
})
export class UserService implements OnInit {
  /*
 Define a variable for NOTES_API_URL and set the value to "http://localhost:3000/notes"
 */
  httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
  };

  constructor(private httpClient: HttpClient) {}

  ngOnInit() {}

  /*
  This function is responsible for fetching notes from db.json
  */
  doLogin(user: User): Observable<User> {
    return this.httpClient
      .post<User>(environment.LOGIN_API_END_POINT, user, this.httpOptions)
      .pipe(
        tap((user: User) => {
          console.log("Post success: " + user)
        }),
        catchError(this.handleErrors)
      );
  }

  private setSession(authResult) {
    //const expiresAt = moment().add(authResult.expiresIn,'second');

    localStorage.setItem('id_token', authResult.idToken);
    //localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
} 

  /*
  This function is responsible for adding note to db.json
  */

  doSignup(user: User): Observable<User> {
    return this.httpClient
      .post<User>(environment.SIGNUP_API_END_POINT, user, this.httpOptions)
      .pipe(
        tap((user: User) => console.log("Post success: " + user)),
        catchError(this.handleErrors)
      );
  }

  // handle error while get notes from server.
  handleGetNotesError(error: Response | any): Observable<User[]> {
    console.log(
      "handleGetNotesError > Eror mesage : " + error.message || error
    );

    let user: User[] = [
      {
        id: "1",
      name: "Error Occured",
      email: "Error Occured",
      username: "Error Occured",
      password: "Error Occured"
      }
    ];
    return of(user);
  }

  // handle error while post a note to the server.
  handleErrors(error: Response | any) {
    console.log("handleAddNoteErrors> Eror mesage : " + error.message || error);
    let user: User = {
      id: "1",
      name: "Error Occured",
      email: "Error Occured",
      username: "Error Occured",
      password: "Error Occured"
    };
    return of(user);
  }
}
